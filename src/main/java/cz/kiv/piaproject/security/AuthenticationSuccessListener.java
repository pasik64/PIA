package cz.kiv.piaproject.security;

import cz.kiv.piaproject.model.User;
import cz.kiv.piaproject.service.FriendshipService;
import cz.kiv.piaproject.service.UserService;
import cz.kiv.piaproject.web.dto.FriendDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AuthenticationSuccessListener implements ApplicationListener<InteractiveAuthenticationSuccessEvent> {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private UserService userService;

    @Autowired
    private FriendshipService friendshipService;

    @Override
    public void onApplicationEvent(InteractiveAuthenticationSuccessEvent event) {
        List<User> onlineFriends = userService.getOnlineFriends();

        List<FriendDto> friendDtoList = friendshipService.getFriendsFromUsers(onlineFriends);
        for (FriendDto friendDto : friendDtoList) {
            Long friendId = friendDto.getFriendId();
            friendDto.setFriendName(userService.getCurrentUser().getName());
            friendDto.setFriendId(userService.getCurrentUser().getId());

            simpMessagingTemplate.convertAndSend("/user/" + friendId + "/friendlist/login/", friendDto);
        }
    }
}
