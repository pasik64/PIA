package cz.kiv.piaproject.service;

import cz.kiv.piaproject.model.Post;
import cz.kiv.piaproject.model.User;
import cz.kiv.piaproject.repository.PostRepository;
import cz.kiv.piaproject.repository.UserRepository;
import cz.kiv.piaproject.web.dto.FriendDto;
import cz.kiv.piaproject.web.dto.PostDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService{

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    private final int RETURN_POST_COUNT = 20;

    @Override
    public Post sendPost(String postText, boolean isAnnouncement) {
        if (isAnnouncement && getCurrentUser().getRoles().stream().noneMatch(r -> r.getName().equals("ROLE_ADMIN"))) {
            return null;
        }
        Post post = new Post();
        post.setText(postText);
        post.setUserId(getCurrentUser().getId());
        post.setDate(new Date());
        post.setAnnouncement(isAnnouncement);
        return postRepository.save(post);
    }

    @Override
    public List<PostDto> getPosts(List<FriendDto> friendDtos) {
        return getPosts(friendDtos, 0, 0);
    }

    @Override
    public List<PostDto> getPosts(List<FriendDto> friendDtos, int page, int offset) {
        int calculatedPage = (offset % RETURN_POST_COUNT) + page;
        int pageOffset = offset - (offset % RETURN_POST_COUNT) * RETURN_POST_COUNT;
        List<Long> userIds = friendDtos.stream().map(FriendDto::getFriendId).collect(Collectors.toList());
        userIds.add(getCurrentUser().getId());
        List<Post> posts = postRepository.findAllByUserIdInOrAnnouncementIsTrueOrderByDateDesc(userIds, PageRequest.of(calculatedPage, RETURN_POST_COUNT));
        if (posts.isEmpty()) {
            return null;
        }
        return transformToDto(posts.subList(pageOffset,posts.size() - 1));
    }

    public User getCurrentUser() {
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepository.findByEmail(user.getUsername());
    }

    private List<PostDto> transformToDto(List<Post> posts) {
        List<PostDto> transformedList = new ArrayList<>();
        List<Long> userIds = posts.stream().map(Post::getUserId).collect(Collectors.toList());
        List<User> usersList = userRepository.findAllById(userIds);
        for (Post post : posts) {
            PostDto postDto = new PostDto();
            User user = usersList.stream().filter(u -> Objects.equals(u.getId(), post.getUserId())).findFirst().orElse(null);
            if (user == null && post.isAnnouncement()) {
                user = userRepository.getById(post.getUserId());
            }
            postDto.setAnnouncement(post.isAnnouncement());
            assert user != null;
            postDto.setFriendName(user.getName());
            postDto.setText(post.getText());
            postDto.setDate(post.getDate());
            postDto.setId(post.getId());
            transformedList.add(postDto);
        }
        return transformedList;
    }
}
