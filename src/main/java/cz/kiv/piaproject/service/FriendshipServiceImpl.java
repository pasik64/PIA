package cz.kiv.piaproject.service;

import cz.kiv.piaproject.enums.FriendshipStatus;
import cz.kiv.piaproject.model.Friendship;
import cz.kiv.piaproject.model.User;
import cz.kiv.piaproject.repository.FriendshipRepository;
import cz.kiv.piaproject.repository.MessageRepository;
import cz.kiv.piaproject.repository.UserRepository;
import cz.kiv.piaproject.web.dto.FriendDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class FriendshipServiceImpl implements FriendshipService{

    @Autowired
    private FriendshipRepository friendshipRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Override
    public List<Friendship> getFriendshipList() {
        return friendshipRepository.findAllByUserId(getCurrentUser().getId());
    }

    @Override
    public Friendship sendFriendRequest(Long friendId) {
        Friendship friendship = new Friendship();
        User currentUser = getCurrentUser();

        if (friendshipRepository.findByUserIdAndFriendId(currentUser.getId(), friendId) != null) {
            return null;
        }

        User friend = userRepository.findById(friendId).orElse(null);
        friendship.setFriendId(friendId);
        assert friend != null;
        friendship.setFriendName(friend.getName());
        friendship.setUserId(currentUser.getId());
        friendship.setFriendshipStatus(FriendshipStatus.PENDING);
        friendshipRepository.save(friendship);

        return friendship;
    }

    @Override
    public FriendDto acceptFriendShip(Long friendshipId) {
        Friendship friendship = friendshipRepository.findById(friendshipId).orElse(null);

        if (friendship == null) {
            return null;
        }

        friendship.setFriendshipStatus(FriendshipStatus.FRIENDS);
        friendshipRepository.save(friendship);
        return transformFriendshipToDto(friendship);
    }

    @Override
    public void declineFriendship(Long friendshipId) {
        Friendship friendship = friendshipRepository.findById(friendshipId).orElse(null);

        if (friendship == null) {
            return;
        }

        friendshipRepository.deleteById(friendship.getId());
    }

    @Override
    public Friendship blockFriendship(Long friendshipId) {
        Friendship friendship = friendshipRepository.findById(friendshipId).orElse(null);
        assert friendship != null;
        if (getCurrentUser().getId().equals(friendship.getUserId())) {
            friendship.setFriendshipStatus(FriendshipStatus.BLOCKED);
            friendshipRepository.save(friendship);
            return friendship;
        } else {
            friendshipRepository.deleteById(friendshipId);

            Friendship blocked = new Friendship();
            blocked.setFriendshipStatus(FriendshipStatus.BLOCKED);
            blocked.setUserId(getCurrentUser().getId());
            blocked.setFriendId(friendship.getUserId());
            blocked.setFriendName(Objects.requireNonNull(userRepository.findById(friendship.getUserId()).orElse(null)).getName());
            friendshipRepository.save(blocked);
            return blocked;
        }
    }

    @Override
    public void unblockFriendship(Long friendshipId) {
        Friendship friendship = friendshipRepository.findById(friendshipId).orElse(null);

        if (friendship == null) {
            return;
        }

        friendshipRepository.deleteById(friendship.getId());
    }

    @Override
    public List<FriendDto> getFriendsFromUsers(List<User> userList) {
        List<Long> userIds = userList.stream().map(User::getId).collect(Collectors.toList());

        List<Friendship> friendships = friendshipRepository.findAllByUserIdInAndFriendId(userIds,getCurrentUser().getId());
        friendships.addAll(friendshipRepository.findAllByUserIdAndFriendIdIn(getCurrentUser().getId(), userIds));


        return transformFriendshipsToDto(friendships);
    }

    @Override
    public List<FriendDto> getFriendsList() {
        User currentUser = getCurrentUser();
        List<Friendship> friendships = friendshipRepository.findAllByUserIdAndFriendshipStatus(currentUser.getId(), FriendshipStatus.FRIENDS);
        friendships.addAll(friendshipRepository.findAllByFriendIdAndFriendshipStatus(currentUser.getId(), FriendshipStatus.FRIENDS));
        return transformFriendshipsToDto(friendships);
    }

    @Override
    public List<FriendDto> getPendingList() {
        User currentUser = getCurrentUser();

        return transformFriendshipsToDto(friendshipRepository.findAllByFriendIdAndFriendshipStatus(currentUser.getId(), FriendshipStatus.PENDING));
    }

    @Override
    public List<Friendship> getBlockedList() {
        User currentUser = getCurrentUser();

        return friendshipRepository.findAllByUserIdAndFriendshipStatus(currentUser.getId(), FriendshipStatus.BLOCKED);
    }


    private List<FriendDto> transformFriendshipsToDto(List<Friendship> friendships) {
        List<FriendDto> friendDtoList = new ArrayList<>();

        for (Friendship friendship : friendships) {
            friendDtoList.add(transformFriendshipToDto(friendship));
        }

        return friendDtoList;
    }

    private FriendDto transformFriendshipToDto(Friendship friendship) {
        FriendDto friendDto = new FriendDto();
        friendDto.setFriendshipId(friendship.getId());
        User friend;
        if (Objects.equals(friendship.getFriendId(), getCurrentUser().getId())) {
            friend = userRepository.findById(friendship.getUserId()).orElse(null);
        } else {
            friend = userRepository.findById(friendship.getFriendId()).orElse(null);
        }

        if (friend != null) {
            friendDto.setFriendId(friend.getId());
            friendDto.setFriendName(friend.getName());
        }

        List<Long> friendsIds = Arrays.asList(friendDto.getFriendId(), getCurrentUser().getId());
        friendDto.setMessages(messageRepository.findAllByUserFromInAndUserToInOrderByDateAsc(friendsIds, friendsIds));

        assert friend != null;
        friendDto.setAdmin(friend.getRoles().stream().anyMatch(r -> r.getName().equals("ROLE_ADMIN")));
        return friendDto;
    }

    private User getCurrentUser() {
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepository.findByEmail(user.getUsername());
    }
}
