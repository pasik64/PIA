package cz.kiv.piaproject.service;

import cz.kiv.piaproject.model.Message;

public interface MessageService {

    public Message saveMessage(Long userFrom, Long userTo, String message);
}
