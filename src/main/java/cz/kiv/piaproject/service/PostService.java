package cz.kiv.piaproject.service;


import cz.kiv.piaproject.model.Post;
import cz.kiv.piaproject.web.dto.FriendDto;
import cz.kiv.piaproject.web.dto.PostDto;

import java.util.List;

public interface PostService {

    Post sendPost(String post, boolean isAnnouncement);

    List<PostDto> getPosts(List<FriendDto> friendDtos);

    List<PostDto> getPosts(List<FriendDto> friendDtos, int page, int offset);
}
