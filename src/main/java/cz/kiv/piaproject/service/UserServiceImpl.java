package cz.kiv.piaproject.service;

import cz.kiv.piaproject.enums.FriendshipStatus;
import cz.kiv.piaproject.model.Friendship;
import cz.kiv.piaproject.model.Role;
import cz.kiv.piaproject.model.User;
import cz.kiv.piaproject.repository.FriendshipRepository;
import cz.kiv.piaproject.repository.UserRepository;
import cz.kiv.piaproject.web.dto.UserRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FriendshipRepository friendshipRepository;

    @Autowired
    private SessionRegistry sessionRegistry;

    public User findByEmail(String email){
        return userRepository.findByEmail(email);
    }

    public User save(UserRegistrationDto registration){
        User user = new User();
        user.setName(registration.getName());
        user.setEmail(registration.getEmail());
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        user.setPassword(encoder.encode(registration.getPassword()));
        user.setRoles(Collections.singletonList(new Role("ROLE_USER")));
        return userRepository.save(user);
    }

    @Override
    public List<User> searchUsers(String namePref) {
        List<User> users = userRepository.findAllByNameContains(namePref);
        List<Friendship> friendships = friendshipRepository.findAllByUserId(getCurrentUser().getId());
        friendships.addAll(friendshipRepository.findAllByFriendId(getCurrentUser().getId()));
        List<Friendship> unwantedFriendships = friendships.stream().filter(f -> f.getFriendshipStatus().equals(FriendshipStatus.BLOCKED) || f.getFriendshipStatus().equals(FriendshipStatus.FRIENDS)).collect(Collectors.toList());
        List<Long> unwantedUsers = new ArrayList<>();
        for (Friendship f : unwantedFriendships) {
            if (!unwantedUsers.contains(f.getUserId())) {
                unwantedUsers.add(f.getUserId());
            }
            if (!unwantedUsers.contains(f.getFriendId())) {
                unwantedUsers.add(f.getFriendId());
            }
        }


        return users.stream().filter(user -> !(unwantedUsers.contains(user.getId()) || user.getId().equals(getCurrentUser().getId()))).collect(Collectors.toList());
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(),
                user.getPassword(),
                mapRolesToAuthorities(user.getRoles()));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles){
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public User getCurrentUser() {
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepository.findByEmail(user.getUsername());
    }

    @Override
    public boolean isCurrentUserAdmin() {
        User user = getCurrentUser();
        for (Role role : user.getRoles()) {
            if (role.getName().equals("ROLE_ADMIN")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public User addAdmin(Long userId) {
        if (isCurrentUserAdmin()) {
            User user = userRepository.getById(userId);
            Collection<Role> roles = user.getRoles();
            Role role = new Role();
            role.setName("ROLE_ADMIN");
            roles.add(role);
            user.setRoles(roles);
            return userRepository.save(user);
        } else return null;
    }

    @Override
    public User removeAdmin(Long userId) {
        if (isCurrentUserAdmin()) {
            User user = userRepository.getById(userId);
            Collection<Role> roles = user.getRoles();
            roles.removeIf(role -> role.getName().equals("ROLE_ADMIN"));
            user.setRoles(roles);
            return userRepository.save(user);
        } else return null;
    }

    @Autowired
    public List<String> findAllLoggedInUsers() {
        return sessionRegistry.getAllPrincipals()
                .stream()
                .filter(principal -> principal instanceof UserDetails)
                .map(UserDetails.class::cast).map(UserDetails::getUsername)
                .collect(Collectors.toList());
    }

    @Override
    public List<User> getOnlineFriends() {
        User currentUser = getCurrentUser();
        List<Friendship> friendships = friendshipRepository.findAllByUserIdOrFriendIdAndFriendshipStatus(currentUser.getId(), currentUser.getId(), FriendshipStatus.FRIENDS);
        List<Long> friendIds = new ArrayList<>();
        for (Friendship friendship : friendships) {
            if (friendship.getUserId().equals(currentUser.getId())) {
                friendIds.add(friendship.getFriendId());
            } else {
                friendIds.add(friendship.getUserId());
            }
        }
        List<String> loggedUsers = findAllLoggedInUsers();


        return userRepository.findAllById(friendIds).stream().filter(user -> loggedUsers.contains(user.getEmail())).collect(Collectors.toList());
    }
}
