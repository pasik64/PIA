package cz.kiv.piaproject.service;

import cz.kiv.piaproject.model.User;
import cz.kiv.piaproject.web.dto.UserRegistrationDto;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User save(UserRegistrationDto registration);

    List<User> searchUsers(String namePref);

    User getCurrentUser();

    List<String> findAllLoggedInUsers();

    List<User> getOnlineFriends();

    boolean isCurrentUserAdmin();

    User addAdmin(Long userId);

    User removeAdmin(Long userId);
}
