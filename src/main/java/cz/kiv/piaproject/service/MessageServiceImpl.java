package cz.kiv.piaproject.service;

import cz.kiv.piaproject.model.Message;
import cz.kiv.piaproject.model.User;
import cz.kiv.piaproject.repository.MessageRepository;
import cz.kiv.piaproject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class MessageServiceImpl implements MessageService{

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Message saveMessage(Long userFrom, Long userTo, String text) {
        List<Long> ids = Arrays.asList(userFrom, userTo);
        List<Message> messageList = messageRepository.findAllByUserFromInAndUserToInOrderByDateAsc(ids, ids);
        Message message;
        if (messageList.size() == 10) {
            message = messageList.get(0);
        } else {
            message = new Message();
        }
        User sender = userRepository.findById(userFrom).orElse(null);
        if (sender != null) {
            message.setSenderName(sender.getName());
        }

        message.setText(text);
        message.setUserFrom(userFrom);
        message.setUserTo(userTo);
        message.setDate(new Date());

        return messageRepository.save(message);
    }
}
