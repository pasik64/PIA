package cz.kiv.piaproject.service;

import cz.kiv.piaproject.model.Friendship;
import cz.kiv.piaproject.model.User;
import cz.kiv.piaproject.web.dto.FriendDto;

import java.util.List;

public interface FriendshipService {

    public List<Friendship> getFriendshipList();

    public Friendship sendFriendRequest(Long friendId);

    public FriendDto acceptFriendShip(Long friendId);

    void declineFriendship(Long friendId);

    Friendship blockFriendship(Long friendId);

    void unblockFriendship(Long friendId);

    List<FriendDto> getFriendsFromUsers(List<User> userList);

    List<FriendDto> getFriendsList();

    List<FriendDto> getPendingList();

    List<Friendship> getBlockedList();
}
