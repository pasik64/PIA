package cz.kiv.piaproject.enums;

public enum FriendshipStatus {
    FRIENDS,
    PENDING,
    BLOCKED
}
