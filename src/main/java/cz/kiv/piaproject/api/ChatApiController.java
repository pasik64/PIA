package cz.kiv.piaproject.api;

import cz.kiv.piaproject.model.Message;
import cz.kiv.piaproject.service.MessageService;
import cz.kiv.piaproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/chat")
@CrossOrigin(origins = "*")
public class ChatApiController {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private UserService userService;

    @Autowired
    private MessageService messageService;

    @PostMapping("/")
    public Message sendMessage(@RequestParam("message") String text, @RequestParam("userId") Long userId) {

        Message message = messageService.saveMessage(userService.getCurrentUser().getId(), userId, text);
        simpMessagingTemplate.convertAndSend("/user/" + userId + "/chat/", message);

        return message;
    }
}
