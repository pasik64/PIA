package cz.kiv.piaproject.api;

import cz.kiv.piaproject.model.Post;
import cz.kiv.piaproject.service.FriendshipService;
import cz.kiv.piaproject.service.PostService;
import cz.kiv.piaproject.web.dto.FriendDto;
import cz.kiv.piaproject.web.dto.PostDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/post")
@CrossOrigin(origins = "*")
public class PostApiController {

    @Autowired
    private PostService postService;

    @Autowired
    private FriendshipService friendshipService;

    @PostMapping("/")
    public Post sendPost(@RequestParam("post") String post, @RequestParam("isAnnouncement") boolean isAnnouncement) {
        return postService.sendPost(post, isAnnouncement);
    }

    @GetMapping("/")
    public List<PostDto> getPosts(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "offset", required = false) Integer offset) {
        List<FriendDto> friendDtos = friendshipService.getFriendsList();

        if (page != null && offset != null) {
            return postService.getPosts(friendDtos, page, offset);
        } else {
            return postService.getPosts(friendDtos);
        }
    }
}
