package cz.kiv.piaproject.api;

import cz.kiv.piaproject.model.User;
import cz.kiv.piaproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
@CrossOrigin(origins = "*")
public class UserApiController {

    @Autowired
    private UserService userService;

    @PostMapping("/addAdmin")
    public User addAdmin(@RequestParam("userId") Long userId) {
        return userService.addAdmin(userId);
    }

    @PostMapping("/removeAdmin")
    public User removeAdmin(@RequestParam("userId") Long userId) {
        return userService.removeAdmin(userId);
    }
}
