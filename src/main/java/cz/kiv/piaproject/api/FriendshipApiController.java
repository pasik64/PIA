package cz.kiv.piaproject.api;

import cz.kiv.piaproject.model.Friendship;
import cz.kiv.piaproject.model.User;
import cz.kiv.piaproject.service.FriendshipService;
import cz.kiv.piaproject.service.UserService;
import cz.kiv.piaproject.web.dto.FriendDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/friendship")
@CrossOrigin(origins = "*")
public class FriendshipApiController {

    @Autowired
    private UserService userService;

    @Autowired
    private FriendshipService friendshipService;

    @GetMapping("/search/{pref}")
    public List<User> searchUsers(@PathVariable("pref") String prefix) {
        return userService.searchUsers(prefix);
    }

    @PostMapping("/friendrequest")
    @CrossOrigin
    public Friendship sendFriendRequest(@RequestParam("friendId") Long friendId) {
        return friendshipService.sendFriendRequest(friendId);
    }

    @PostMapping("/accept")
    @CrossOrigin
    public FriendDto acceptFriendship(@RequestParam("friendId") Long friendId) {
        return friendshipService.acceptFriendShip(friendId);
    }

    @PostMapping("/decline")
    @CrossOrigin
    public void declineFriendship(@RequestParam("friendId") Long friendId) {
        friendshipService.declineFriendship(friendId);
    }

    @PostMapping("/block")
    public Friendship blockFriendship(@RequestParam("friendId") Long friendId) {
        return friendshipService.blockFriendship(friendId);
    }

    @PostMapping("/unblock")
    @CrossOrigin
    public void unblockFriendship(@RequestParam("friendId") Long friendId) {
        friendshipService.unblockFriendship(friendId);
    }

    @GetMapping("/onlineFriends")
    public List<User> getOnlineFriends() {
        return userService.getOnlineFriends();
    }
}
