package cz.kiv.piaproject.web.dto;

import java.util.Date;

public class PostDto {

    private String friendName;

    private String text;

    private Date date;

    private Long id;

    private boolean announcement;

    public boolean isAnnouncement() {
        return announcement;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAnnouncement(boolean announcement) {
        this.announcement = announcement;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
