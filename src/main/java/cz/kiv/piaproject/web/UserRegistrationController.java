package cz.kiv.piaproject.web;

import cz.kiv.piaproject.model.User;
import cz.kiv.piaproject.service.UserService;
import cz.kiv.piaproject.web.dto.UserRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.util.StringUtils;

import javax.validation.Valid;
import java.util.regex.Pattern;

@Controller
@RequestMapping("/registration")
public class UserRegistrationController {

    private final String emailRegex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";

    @Autowired
    private UserService userService;

    @ModelAttribute("user")
    public UserRegistrationDto userRegistrationDto() {
        return new UserRegistrationDto();
    }

    @GetMapping
    public String showRegistrationForm(Model model) {
        return "registration";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") @Valid UserRegistrationDto userDto,
                                      BindingResult result){

        if (StringUtils.isEmptyOrWhitespace(userDto.getEmail())) {
            result.rejectValue("email", null, "No email provided");
        }

        if (!Pattern.compile(emailRegex)
                .matcher(userDto.getEmail())
                .matches()) {
            result.rejectValue("email", null, "Not a valid email address");
        }

        if (StringUtils.isEmptyOrWhitespace(userDto.getName())) {
            result.rejectValue("name", null, "No name provided");
        }

        if (StringUtils.isEmptyOrWhitespace(userDto.getPassword())) {
            result.rejectValue("password", null, "No password provided");
        }

        if (!userDto.getEmail().equals(userDto.getConfirmEmail())) {
            result.rejectValue("email", null, "Emails must match");
        }

        if (!userDto.getPassword().equals(userDto.getConfirmPassword())) {
            result.rejectValue("password", null, "Passwords must match");
        }


        User existing = userService.findByEmail(userDto.getEmail());
        if (existing != null){
            result.rejectValue("email", null, "There is already an account registered with that email");
        }

        if (result.hasErrors()){
            return "registration";
        }

        userService.save(userDto);
        return "redirect:/registration?success";
    }

}
