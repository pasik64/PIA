package cz.kiv.piaproject.web;

import cz.kiv.piaproject.service.FriendshipService;
import cz.kiv.piaproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FriendshipController {

    @Autowired
    private FriendshipService friendshipService;

    @Autowired
    private UserService userService;

    @GetMapping("/friendship")
    public ModelAndView friendshipPage() {
        ModelAndView mv = new ModelAndView("friendship-management");
        mv.addObject("friends", friendshipService.getFriendsList());
        mv.addObject("pendings", friendshipService.getPendingList());
        mv.addObject("blocked", friendshipService.getBlockedList());
        mv.addObject("isAdmin", userService.isCurrentUserAdmin());
        return mv;
    }

}
