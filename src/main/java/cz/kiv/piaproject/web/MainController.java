package cz.kiv.piaproject.web;

import cz.kiv.piaproject.model.User;
import cz.kiv.piaproject.service.FriendshipService;
import cz.kiv.piaproject.service.PostService;
import cz.kiv.piaproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class MainController {

    @Autowired
    private UserService userService;

    @Autowired
    private PostService postService;

    @Autowired FriendshipService friendshipService;

    @GetMapping("/")
    public ModelAndView root() {
        ModelAndView mv = new ModelAndView("index");
        mv.addObject("user", userService.getCurrentUser());
        List<User> onlineFriends = userService.getOnlineFriends();

        mv.addObject("friends", friendshipService.getFriendsFromUsers(onlineFriends));
        mv.addObject("posts", postService.getPosts(friendshipService.getFriendsList()));
        mv.addObject("isAdmin", userService.isCurrentUserAdmin());
        return mv;
    }

    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }

    @GetMapping("/user")
    public String userIndex() {
        return "user/index";
    }
}
