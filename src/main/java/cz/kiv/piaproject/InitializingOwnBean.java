package cz.kiv.piaproject;

import cz.kiv.piaproject.model.Role;
import cz.kiv.piaproject.model.User;
import cz.kiv.piaproject.repository.RoleRepository;
import cz.kiv.piaproject.repository.UserRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.Collections;

@Component
public class InitializingOwnBean implements InitializingBean {

    Logger logger = LoggerFactory.getLogger(InitializingOwnBean.class);

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void afterPropertiesSet() throws Exception {
        if (roleRepository.findAllByName("ROLE_ADMIN").isEmpty()) {
            User oldUser = userRepository.findByEmail("admin@admin.cz");
            Role role = new Role();
            role.setName("ROLE_ADMIN");
            if (oldUser != null) {
                oldUser.setRoles(Collections.singleton(role));
                userRepository.save(oldUser);
            } else {
                User user = new User();
                user.setName("admin");
                user.setEmail("admin@admin.cz");
                user.setRoles(Collections.singleton(role));
                String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?";
                String password = RandomStringUtils.random(10, characters);
                user.setPassword(passwordEncoder.encode(password));
                logger.info("No admin present, creating one");
                logger.info("email: admin@admin.cz, password: " + password);
                userRepository.save(user);
            }
        }
    }
}
