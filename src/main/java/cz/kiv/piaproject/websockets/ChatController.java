package cz.kiv.piaproject.websockets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;


@Controller
public class ChatController {
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @MessageMapping("/friendlist")
    public void sendSpecific(Message msg) throws Exception {

        simpMessagingTemplate.convertAndSend("/user/" + msg.getTo() + "/friendlist/", "Hello " + msg.getFrom());
    }
}
