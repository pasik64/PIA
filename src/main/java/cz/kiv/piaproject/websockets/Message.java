package cz.kiv.piaproject.websockets;

public class Message {

    private Long from;
    private Long to;
    private String text;

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }

    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
}