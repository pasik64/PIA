package cz.kiv.piaproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PiaProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(PiaProjectApplication.class, args);
    }

}
