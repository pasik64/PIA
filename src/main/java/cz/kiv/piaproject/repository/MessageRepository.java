package cz.kiv.piaproject.repository;

import cz.kiv.piaproject.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {

    List<Message> findAllByUserFromInAndUserToInOrderByDateAsc(List<Long> usersFrom, List<Long> usersTo);
}
