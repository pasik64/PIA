package cz.kiv.piaproject.repository;

import cz.kiv.piaproject.enums.FriendshipStatus;
import cz.kiv.piaproject.model.Friendship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendshipRepository extends JpaRepository<Friendship, Long> {

    List<Friendship> findAllByUserId(Long userId);

    List<Friendship> findAllByUserIdInAndFriendId(List<Long> userId, Long friendId);

    List<Friendship> findAllByUserIdAndFriendIdIn(Long userId, List<Long> friendId);

    List<Friendship>findAllByFriendId(Long friendId);

    List<Friendship> findAllByUserIdOrFriendIdAndFriendshipStatus(Long userId, Long friendId, FriendshipStatus friendshipStatus);

    Friendship findByUserIdAndFriendId(Long userId, Long friendId);

    List<Friendship> findAllByFriendIdAndFriendshipStatus(Long userId, FriendshipStatus friendshipStatus);

    List<Friendship> findAllByUserIdAndFriendshipStatus(Long userId, FriendshipStatus friendshipStatus);
}
