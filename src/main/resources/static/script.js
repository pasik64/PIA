function searchUsers() {
    var searchUrl = "api/friendship/search/" + document.getElementById('jmenoInput').value;

    $.get(
        searchUrl,
        function(data) {
            document.getElementById("usersList").innerHTML = "";
            data.forEach(element => document.getElementById("usersList").innerHTML += "<div class=\"row\"><div class=\"col-md-6 text-right\">" + element.name + "</div><div class=\"col-md-6\"><input type='button' class=\"btn btn-default\" id='addfriend-" + element.id + "' value='Add friend' onclick='addFriend(" + element.id + ")'/></div></div>"
            );

        }
    );
}

function addFriend(fId) {
    $.post("api/friendship/friendrequest", {friendId: fId}).done(
            function(data) {
                document.getElementById("addfriend-" + fId).setAttribute("disabled", "disabled");
            }
        );
}

