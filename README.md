# PIA - semestrální práce

## O aplikaci
Aplikace je tvořená pomocí java frameworku Spring.
Dále je zde použit šablonovací nástroj Thymeleaf a javascript, pro interakci na klientské části.

## Řešení
Pro zabezpečení aplikace je rozdělena do několika packagí, nejdůležitější jsou api, model, service a web.

### Api
Tento package zařizuje Restovou komunikaci pomocí api. Nacházejí se zde celkem 4 controllery
(ChatApiController, FriendshipApiController, PostApiController a UserApiController), jejichž zaměření 
je patrné z jejich názvu.

### Model
Model slouží pro vytváření objektů v databázi a práci s nimi. Model obsahuje celkem 6 tabulek (friendship,
message, post, role, user, users_roles)

### Service
Service slouží k provádění aplikační logiky, kdy jsou volány z controllerů a komunikují pomocí Repository s databází.

### Web
Controllery ve webu slouží k přípravě dat pro zobrazení na klientu, kdy si pomocí service vyžádají potřebná data
a vloží je do modelu, který je předán šabloně.

## Zabezpečení
Zabezpečení aplikace je řešeno pomocí Spring security. Hesla jsou do databáze ukládána zahashovaná, 
aby nebylo možné jejich přečtení z ní. O zabezpečení jako celek se stará package Security.

## Klient
Klientská část je zařízena pomocí Thymeleaf. Ve složce resources jsou šablony jednotlivých stránek. 

## Spuštění
Ke spuštění aplikace se používá docker.

Pro spuštění aplikace je zapotřebí vstoupit do kořenové složky projektu a zde postupně spustit příkazy: 
- sudo chmod +x mvnw (nemusí být nutné, pouze v případě chyby typu permission denied)
- sudo docker compose build
- sudo docker compose up

Po prvním spuštění se v databázi automaticky vytvoří administrátorský účet, jehož přihlašovací údaje se zalogují do konzole.

## Vytvořená rozšíření

- ukládání zpráv chatu do DB (5 bodů)
- zobrazování starších příspěvků při doscrollování na konec stránky (7 bodů)
- Průběžné používání gitlab repozitáře (2 body)
